
import SnapKit
import TinkoffID
import UIKit

public final class MyController: UIViewController {
    private var authInitializer: ITinkoffAuthInitiator? = PaymentModel.shared.tinkoffId
    private let credentialsRefresher: ITinkoffCredentialsRefresher? = PaymentModel.shared.tinkoffId
    private let signOutInitializer: ITinkoffSignOutInitiator? = PaymentModel.shared.tinkoffId
    
    lazy var signInButton = TinkoffIDButtonBuilder.build(
        configuration: TinkoffIDButtonConfiguration(size: .large),
        title: "Войти с "
    )
    private var credentials: TinkoffTokenPayload? {
        didSet {

        }
    }
    
    @objc
    private func signInButtonTapped() {
        if authInitializer == nil {
        authInitializer = PaymentModel.shared.tinkoffId
        }
        authInitializer!.startTinkoffAuth(handleSignInResult)
    }
    
    private func handleSignInResult(_ result: Result<TinkoffTokenPayload, TinkoffAuthError>) {
        do {
            credentials = try result.get()
        } catch TinkoffAuthError.cancelledByUser {
            print("❌ Auth process cancelled by a user")
        } catch {
            print("❌ \(error)")
        }
    }
    
   
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUserInterface()
    }
    
}

extension MyController: IAuthWebViewSourceProvider {
    public func getSourceViewController() -> UIViewController {
        self
    }
    
    private func setupUserInterface() {
        view.backgroundColor = .white
        
        signInButton.addTarget(self, action: #selector(signInButtonTapped), for: .touchUpInside)
        view.addSubview(signInButton)
        
      
        signInButton.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalTo(view.safeAreaLayoutGuide).inset(16)
        }
        
    }
    
}
