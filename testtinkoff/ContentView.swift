
import SwiftUI
import WebKit


struct ContentView: View {
    @State var showController = false
    var body: some View {
        VStack {
            Button("start", action: {
                showController.toggle()
            })
                .sheet(isPresented: $showController, content: {
                    MyControllerRepresentable()
                })
            
        }
        .padding()
    }
}
