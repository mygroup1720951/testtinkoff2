    import SwiftUI
    import TinkoffID



    @main
    struct testtinkoffApp: App {
        
        @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
        var body: some Scene {
            WindowGroup {
                ContentView()
            }
        }
    }

    class AppDelegate: NSObject, UIApplicationDelegate {
        
        lazy var tinkoffId: ITinkoffID = {
            let clientId = "tid_zarabotay-mb"
            let callbackUrl = "zarabotay://auth/tinkoff"
            assert(!clientId.isEmpty, "Please specify an client ID")
            let factory = TinkoffIDFactory(
                clientId: clientId,
                callbackUrl: callbackUrl
                ,webViewSourceProvider: myController
            )
            return factory.build()
        }()

        lazy var myController: MyController = {
            MyController()
        }()

        func application(_ app: UIApplication,
                         open url: URL,
                         options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            return tinkoffId.handleCallbackUrl(url)
        }

        func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
            PaymentModel.shared.tinkoffId = tinkoffId
                return true
            }
    }
