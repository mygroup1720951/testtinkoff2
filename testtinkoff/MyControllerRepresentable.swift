import SwiftUI

struct MyControllerRepresentable: UIViewControllerRepresentable {
    typealias UIViewControllerType = MyController
    
    func makeUIViewController(context: Context) -> MyController {
    let vc = MyController()
        return vc
    }
    func updateUIViewController(_ uiViewController: MyController, context: Context) {
        
    }
}
